#include<stdio.h>

int main()
{
	int j;
	
	printf("print odd number between 1 to 100");
	for(int i=1;i<=100;i++)
	{
		if(i%2==1)
			printf("%d\n",i);
	}

	printf("print even number between 1 to 100");
	for(int i=1;i<=100;i++)
	{
		if(i%2==0)
			printf("%d\n",i);
	}

	printf("print prime number between 1 to 100");
	for(int i=1;i<=100;i++)
	{
		for(int j=2;j<=i;j++)
		{
			if(i%j==0)
				break;
		}
		if(i==j)
			printf("%d\n",i);
	}	

	return 0;
}
